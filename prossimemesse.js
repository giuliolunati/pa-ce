// library
var Giorni= [
  "Dom",
  "Lun",
  "Mar",
  "Mer",
  "Gio",
  "Ven",
  "Sab"
]

var formatData= function (data) {
  let mesi= [
    "Gennaio",
    "Febbraio",
    "Marzo",
    "Aprile",
    "Maggio",
    "Giugno",
    "Luglio",
    "Agosto",
    "Settembre",
    "Ottobre",
    "Novembre",
    "Dicembre"
  ]
  let s= (
    Giorni[data.getDay()]+" "+
    data.getDate()+" "+
    mesi[data.getMonth()]+" "+
    (1900 + data.getYear())
  )
  let h= data.getHours()
  let m= data.getMinutes()
  if (h + m > 0) {
    if (m < 10) m= "0"+m
    s+= ", ore "+h+":"+m
  }
  return s
}

var TimeDelay= 0
var now= function () {
  return Date.now() + TimeDelay
}

var ids= [
  "sanmichele",
  "cattedrale",
  "santeodoro",
  "sanfrancesco",
  "betlem",
  "carmine",
  "sangervasio",
  "sanprimo",
  "cieldoro",
  "canepanova",
  "domnarum",
  "borromeo",
  "sangiorgio"
]

var Chiese= {}

var refresh= function () {
  fetch("now.php", {cache: "no-store"})
  .then(res => res.text())
  .then(text => {
    // PHP seconds => JS milliseconds
    TimeDelay= 1000 * parseInt(text) - now()
  })
  .then(()=>{
    fetches= []
    for (let id of ids) {
      fetches.push(
        fetch("h/"+id+".json", {cache: "no-store"})
        .then(res => res.json())
        .then(json => {Chiese[id]= json})
      )
    }
    Promise.all(fetches)
    .then(res => {
      mostraMesse()
    })
  })
}

var mostraMesse= function (day) {
  let time= now() 
  let data= new Date(time)
  let d= data.getDay()
  let div= ""
  for (let i=0; i<7; i++) {
    let j= (i + d + 7) % 7
    div += "<input value='"+Giorni[j]+"' type='button' onclick=\"mostraMesse(this.value)\"/>"
  }
  document.getElementById("settimana").innerHTML= div
  day= Giorni.indexOf(day)
  if (day >= 0) {
    day -= data.getDay()
    if (day < 0) day += 7
    time += day * 24000 * 3600
    data= new Date(time)
    if (day > 0) {
      data.setHours(0)
      data.setMinutes(0)
    }
  }
  document.getElementById("oggi").innerHTML= formatData(data)

  let m=[]
  for (let id in Chiese) {
    let ch= Chiese[id]
    let sem= ch.semaforo
    for (let k in ch) {

      let v= ch[k]
      if (Array.isArray(v) &&
        giornoGiusto(data, k)
      ) {
        for (let ora of v) {
          if (ora.length == 4)
            ora= " "+ora
          // la messa "scade" 20 min dopo l'inizio 
          let tra= traQuanto(data, ora)
          if (tra > -20) {
            if (tra > 60) sem= 0
            m.push([ora, id, sem])
          }
        }
      }
    }
  }
  m.sort()
  let displayLegenda= "none"
  let html=""
  for (let r of m) {
    let color
    if (r[2]) { // semaforo
      displayLegenda= "block"
      color= ["", "lightgreen", "darkorange", "red"][r[2]]
    }
    html += "<tr"
    if (color) html += " style='background: "+color+"'"
    html += "><td style='text-align: right'>"+r[0]+"</td>"
    + "<td>"+Chiese[r[1]].nome+"</td></tr>"

  }
  if (m.length > 0) {
    html= "<table><tr class='hi'><td>Ora</td>"
    +"<td>Parrocchia</td></tr>"+html+"</table>"
  } else {
    html= "(Oggi non ci sono più messe)"
  }
  document.getElementById("messe").innerHTML= html
  document.getElementById("legenda").style.display= displayLegenda
}

var giornoGiusto= function (data, desc) {
  let giorno= data.getDay()
  if (desc == "lun mar gio ven")
    return (
      giorno == 1 ||
      giorno == 2 ||
      giorno == 4 ||
      giorno == 5
    )
  if (desc == "mer sab")
    return (giorno == 3 || giorno == 6)
  if (giorno == 0) {
    return (
      desc == "festive" ||
      desc == "dom"
    )
  }
  if (
    giorno == 6 &&
    data.getHours() >= 14
  ) {
    return (desc == "prefestive")
  }
  return (
    desc == "feriali"
  )
}

var traQuanto= function (data, ora) {
  let a= 60 * data.getHours() + data.getMinutes()
  let b= ora.split(":")
  b= 60 * parseInt(b[0]) + parseInt(b[1])
  return b - a
}

// vim: set et sw=2:
