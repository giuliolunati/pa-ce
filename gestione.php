<!DOCTYPE html>
<html>
<head>

  <title>Gestione Messe centro città</title>
  <link href="" rel="icon" />
  <link href="common.css?8" rel="stylesheet" />

  <meta charset="UTF-8" />
  <meta name="viewport" content=
    "width=device-width, initial-scale=1, minimum-scale=0.3, maximum-scale=3"
  />

  <script>
    onerror= (e,f,r,c)=>{
    alert("\n"+e+"\nin "+f+"\nat row "+r+", col "+c)}
  </script>
  <script>
    var fix_form= function(o) {
      o.id.value= o.id.value.trim().toLowerCase()
      o.pw.value= o.pw.value.trim().toLowerCase()

    }
  </script>
</head>
<body>

<?php

function wrong_pw($pw, $hash) {
  if (password_verify($pw, $hash)) return false;
  else return password_hash($pw, PASSWORD_DEFAULT);
}

// MAIN //
$expiretime= 3600; // semaforo expire after 1h
if (! $_POST) $_POST= $_GET;
$id= $pw= $semaforo= null;
if (array_key_exists("id", $_POST)) $id= $_POST["id"];
if (array_key_exists("pw", $_POST)) $pw= $_POST["pw"];
if (array_key_exists("semaforo", $_POST)) $semaforo= $_POST["semaforo"];
$obj= [];
$fn= "h/".$id.".json";
if (file_exists($fn)) {
  $obj= file_get_contents($fn);
  $obj= json_decode($obj);
  if (time() > $obj->expiretime) {
    $obj->semaforo= "0";
  }
}

if ($obj && $obj->nome) echo "<h1>{$obj->nome}</h1>";

// login //
if (!$obj || $hash= wrong_pw($pw, $obj->hash)) { ?>
  <form method="POST" onsubmit="fix_form(this)">
  <p>Utente:</p>
  <input name="id"
    value="<?php echo $id?>"
    type="text"
  >
  <p>Password:</p>
  <input name="pw" type="text">
  <?php if ($id == "admin") {
    echo "<textarea style='width: 90%; height: 7ex'>";
    print_r($hash);
    echo "</textarea>";
  } ?>
  <p><input type="submit" value="Accedi"></p>
  </form>
<?php exit(); }

// login ok //
echo "<form method='POST'>\n";
echo "\t<input name='pw' type='hidden' value='$pw'>\n";
echo "\t<input name='id' type='hidden' value='$id'>\n";
// semaforo //
echo "\t<h2>Semaforo</h2>\n";
// cambia semaforo? //
if (isset($semaforo)) {
  $fp= fopen($fn, "w");
  if (flock($fp, LOCK_EX)) {
    $obj->semaforo= $semaforo;
    //var_dump($obj);
    $obj->expiretime= time() + $expiretime;
    fwrite($fp, json_encode($obj, JSON_PRETTY_PRINT));
    flock($fp, LOCK_UN);
  } else {
    echo "<p>Impossibile salvare le modifiche, riprova...</p>";
  }
  fclose($fp);
}
echo "\t<div id='semaforo'>\n";
$stati=["DISATTIVO", "C'È POSTO", "QUASI PIENA", "PIENA"];
for ($i=0; $i<4; $i++) {
  echo "\t\t<button name='semaforo' value='$i'";
 if ($i == $obj->semaforo) echo " style='border: 0.7ex solid white'";
  echo ">{$stati[$i]}</button><br>\n";
};

?>

<p>Attenzione:<br/>il semaforo si disattiva dopo un'ora.</p>

</div>
</form>
</body>
</html>
<!-- vim: set et sw=2: -->
